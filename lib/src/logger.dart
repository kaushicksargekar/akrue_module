import 'package:akrue_module/akrue_module.dart';
import 'package:logger/logger.dart';


void wtfL(val) => Logger().wtf(val);

void infoL(val) => Logger().i(val);

void errorL(val) => Logger().e(val);

void warningL(val) => Logger().w(val);

void debugL(val) => Logger().d(val);

void verboseL(val) => Logger().v(val);

