import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CachedImage extends StatelessWidget {
  final String url;

  const CachedImage({Key key, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: url,
      alignment: Alignment.center,
      fit: BoxFit.fill,
      height: 100,
      width: 100,
      placeholder: (c,s){
        return SpinKitChasingDots(
          color: Colors.black,
        );
      },
    );
  }
}
