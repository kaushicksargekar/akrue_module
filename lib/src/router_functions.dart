import 'package:auto_route/auto_route.dart';

class AppNavigator {
  static void pushA(className, args) =>
      ExtendedNavigator.root.push(className, arguments: args);

  static void popPush(className) =>
      ExtendedNavigator.root.popAndPush(className);

  static void popPushA(className, args) =>
      ExtendedNavigator.root.popAndPush(className, arguments: args);


}
