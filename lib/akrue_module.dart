library akrue_module;

export 'r.g.dart';
export 'res/constants.dart';
export 'src/router_functions.dart';
export 'src/methods.dart';
export 'src/debounce.dart';
export 'src/logger.dart';
export 'src/widgets/widgets.dart';
export 'src/camera_cropper.dart';



//packages
export 'package:cupertino_icons/cupertino_icons.dart';
export 'package:sized_context/sized_context.dart';
export 'package:supercharged/supercharged.dart';
export 'package:auto_route/auto_route.dart';
export 'package:flutter_riverpod/all.dart';
export 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
export 'package:flutter_icons/flutter_icons.dart';
export 'package:validators/sanitizers.dart';
export 'package:validators/validators.dart';
export 'package:flushbar/flushbar.dart';
export 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
export 'package:floating_action_bubble/floating_action_bubble.dart';
export 'package:retrofit/retrofit.dart';
export 'package:awesome_dialog/awesome_dialog.dart';
export 'package:auto_size_text/auto_size_text.dart';
export 'package:webview_flutter/webview_flutter.dart';
export 'package:flutter_spinkit/flutter_spinkit.dart';
export 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
export 'package:async_loader/async_loader.dart';
export 'package:flutter_swiper/flutter_swiper.dart';
export 'package:sliding_sheet/sliding_sheet.dart';
export 'package:dropdown_search/dropdown_search.dart';
